<?php

namespace App;


class Student{

    protected $name;
    protected $studentID;


    public function setData($postArray){

        if(array_key_exists("Name",$postArray)){
            $this->name = $postArray['Name'];
        }
        if(array_key_exists("StudentID",$postArray)){
            $this->studentID = $postArray['StudentID'];
        }
    }// end of setData

    public function getData(){

        $name =$this->name;
        $studentID= $this->studentID;

        $varList = array("name","studentID");

              $studentInfoArray =    compact($varList);
              return $studentInfoArray;

    }




}